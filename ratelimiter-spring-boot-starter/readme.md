### 说明

使用spring boot自动配置flowcontrol

### 例子

```
flow:
  control:
    enabled: 'true' ##是否启用
    filter-url-patterns: /* ##流量控制filter处理的链接
    redis-url: redis://localhost:6379 ##redis地址
    items: ##流量控制具体配置
    - expl: /test/** ##地址表达式，ant风格
      expires: 10000 ##超时时间，单位毫秒
      max-call-times: 20 ##访问上限
      id: 1 ##id
    - expl: /test/f1/**
      expires: 10000
      max-call-times: 10
      id: 2 
```

具体可以查看[样例](https://gitee.com/shiqiyue/flow-control/tree/master/flowcontrol-spring-boot-starter-sample)